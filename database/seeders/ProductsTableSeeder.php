<?php


namespace Database\Seeders;


use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    public function run()
    {
        $product = Factory::factoryForModel(Product::class)::times(10)->create();
    }
}
