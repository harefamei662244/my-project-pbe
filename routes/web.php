<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Str;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('api/v1/login', 'Auth\LoginController@verify');

$router->group(['prefix' => 'api/v1', 'middleware' => 'login.auth'], function ($router) {
    /* Product */
    // Untuk menggambil semua data
    $router->get('/products', 'ProductController@getAll');
    // Untuk mengambil data berdasarkan Id
    $router->get('/products/{id}', 'ProductController@getById');
    // Untuk menambah data
    $router->post('/products', 'ProductController@create');
    // Untuk mengubah data
    $router->put('/products/{id}', 'ProductController@update');
    // Untuk menghapus data
    $router->delete('/products/{id}', 'ProductController@delete');

    /* Cashier */
    // Untuk menggambil semua data
    $router->get('/cashiers', 'CashierController@getAll');
    // Untuk mengambil data berdasarkan Id
    $router->get('/cashiers/{id}', 'CashierController@getById');
    // Untuk menambah data
    $router->post('/cashiers', 'CashierController@create');
    // Untuk mengubah data
    $router->put('/cashiers/{id}', 'CashierController@update');
    // Untuk menghapus data
    $router->delete('/cashiers/{id}', 'CashierController@delete');
});
