<?php


namespace App\Http\Controllers\Base;

use App\Exceptions\LoginNotAuthtenticatedException;
use App\Models\User;
use Laravel\Lumen\Routing\Controller as BaseController;

abstract class LoginBaseController extends BaseController
{
    public function __construct()
    {
        $token = request()->header('api_token');
        $user = User::where('api_token', '=', $token)->first();
        if ($user === null) {
            throw new LoginNotAuthtenticatedException();
        }
    }

    protected function successResponses(array $data, int $httpcode = 200)
    {
        $response = [
            'status' => 'Successed',
            'message' => 'Permintaan berhasil di proses',
            'data' => $data
        ];
        return response()->json($response, $httpcode);
    }

    protected function failResponse(array $data, int $httpcode)
    {
        $response = [
            'status' => 'Failed',
            'message' => 'Permintaan gagal di proses',
            'data' => $data
        ];
        return response()->json($response, $httpcode);
    }
}
