<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Base\LoginBaseController;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends LoginBaseController
{
    /**
     * Function untuk mendapatkan semua data product
     * @return JsonResponse
     */
    public function getAll()
    {
        $products = Product::all();
        return $this->successResponses(['products' => $products]);
    }

    /**
     * Function untuk mengambil 1 data dari product berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getById($id)
    {
        $product = Product::find($id);
        // Jika product tidak ada di database
        if ($product === null) {
            throw new NotFoundHttpException();
        }
        return $this->successResponses(['product' => $product]);
    }

    /**
     * Function untuk menambah data di produk
     * @return JsonResponse
     */
    public function create()
    {
        /* Validasi */
        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        // Jika tidak ada error yang terjadi
        $product = new Product();
        $product->name = request('name');
        $product->price = request('price');
        $product->stock = request('stock');
        $product->save();
        return $this->successResponses(['product' => $product], 201);
    }

    /**
     * Function untuk mengubah data di database
     * @param $id
     * @return JsonResponse
     */
    public function update($id)
    {
        $product = Product::find($id);
        // Jika product tidak ada di database
        if ($product === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi */
        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        // Jika tidak ada error yang terjadi
        $product->name = request('name');
        $product->price = request('price');
        $product->stock = request('stock');
        $product->save();
        return $this->successResponses(['product' => $product]);
    }

    /**
     * Function untuk menghapus data di database
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        $product = Product::find($id);
        // Jika product tidak ada di database
        if ($product === null) {
            throw new NotFoundHttpException();
        }
        $product->delete();
        return $this->successResponses(['product' => 'Data berhasil di hapus.']);
    }
}
