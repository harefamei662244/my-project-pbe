<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Base\LoginBaseController;
use App\Models\Cashier;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CashierController extends LoginBaseController
{
    /**
     * Function untuk mendapatkan semua data cashier
     * @return JsonResponse
     */
    public function getAll()
    {
        $cashier = Cashier::all();
        return $this->successResponses(['cashiers' => $cashier]);
    }

    /**
     * Function untuk mengambil 1 data dari cashier berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getById($id)
    {
        $cashier = Cashier::find($id);
        // Jika cashier tidak ada di database
        if ($cashier === null) {
            throw new NotFoundHttpException();
        }
        return $this->successResponses(['cashier' => $cashier]);
    }

    /**
     * Function untuk menambah data di cashier
     * @return JsonResponse
     */
    public function create()
    {
        /* Validasi */
        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'Jenis_Kelamin' => 'required',
            'tanggal_lahir' => 'required|date',
            'email' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        // Jika tidak ada error yang terjadi
        $cashier = new Cashier();
        $cashier->name = request('name');
        $cashier->Jenis_Kelamin = request('Jenis_Kelamin');
        $cashier->tanggal_lahir = request('tanggal_lahir');
        $cashier->email = request('email');
        $cashier->save();
        return $this->successResponses(['cashier' => $cashier], 201);
    }

    /**
     * Function untuk mengubah data di database
     * @param $id
     * @return JsonResponse
     */
    public function update($id)
    {
        $cashier = Cashier::find($id);
        // Jika cashier tidak ada di database
        if ($cashier === null) {
            throw new NotFoundHttpException();
        }
        /* Validasi */
        $validate = Validator::make(request()->all(), [
            'name' => 'required',
            'Jenis_Kelamin' => 'required',
            'tanggal_lahir' => 'required|date',
            'email' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        // Jika tidak ada error yang terjadi
        $cashier->name = request('name');
        $cashier->Jenis_Kelamin = request('Jenis_Kelamin');
        $cashier->tanggal_lahir = request('tanggal_lahir');
        $cashier->email = request('email');
        $cashier->save();
        return $this->successResponses(['cashier' => $cashier]);
    }

    /**
     * Function untuk menghapus data di database
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        $cashier = Cashier::find($id);
        // Jika cashier tidak ada di database
        if ($cashier === null) {
            throw new NotFoundHttpException();
        }
        $cashier->delete();
        return $this->successResponses(['Cashier' => 'Data berhasil di hapus.']);
    }
}


