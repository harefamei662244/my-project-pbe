<?php


namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Models\User;

class LoginController extends Controller
{
    public function verify()
    {
        $username = $_SERVER['PHP_AUTH_USER'];
        $password = $_SERVER['PHP_AUTH_PW'];
        $user = User::loginVerify($username, $password);
        if ($user !== false) {
            $apiToken = md5($user->username . date('Y-m-d H:i:s'));
            $user->api_token = $apiToken;
            $user->save();
            return $this->successResponses(['user' => $user]);
        }
        return $this->failResponse([], 401);
    }
}
