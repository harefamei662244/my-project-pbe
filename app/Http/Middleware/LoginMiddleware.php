<?php

namespace App\Http\Middleware;

use App\Exceptions\LoginNotAuthtenticatedException;
use Closure;

class LoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('api_token'))) {
            /* Kondisi api token tidak dikirim melalui header */
            throw new LoginNotAuthtenticatedException();
        }
        /* Kondisi api tokennya ada */
        return $next($request);
    }
}
