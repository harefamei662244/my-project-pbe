<?php


namespace App\Exceptions;


class LoginNotAuthtenticatedException extends \Exception
{
    public function render()
    {
        return response()->json([
            'status' => 'Failed',
            'message' => 'Anda tidak terauntentikasi',
        ], 401);
    }
}
